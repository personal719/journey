# Summary
A project for organization journeys .

## Functional requirements
> ##### Add ownerships of farms,chalets,.... .
> ##### Search in journeys ,and ownerships with filters
> ##### Authentication using sms messages 
> ##### Add journeys 
> ##### Log requests

## Non function Requirement
> ##### High performance for search which implemented using the following techniques
>* Cache(implemented via [aspect-oriented programming (AOP)](https://en.wikipedia.org/wiki/Aspect-oriented_programming#:~:text=In%20computing%2C%20aspect%2Doriented%20programming,separation%20of%20cross%2Dcutting%20concerns.&text=Aspect%2Doriented%20programming%20entails%20breaking,%2C%20cohesive%20areas%20of%20functionality).))
>* Using search engine database([SOLR](https://solr.apache.org/))
>* Using batch insert into database instead of insert records one by one  which means insert multiple record in database instead of one by one to save too many connections to database .
>* Using [Thread pool](https://en.wikipedia.org/wiki/Thread_pool) to set some tasks in background 
>##### Security
>* Not implemented yet but it will be using [RBAC SYSTEM](https://en.wikipedia.org/wiki/Role-based_access_control)

##Used tools and techniques
* Mysql(8.0)
* Java(1.8)
* Solr (7.7.3)
* Design patterns
    * [Facade](https://refactoring.guru/design-patterns/facade)
    * [Factory Method](https://refactoring.guru/design-patterns/factory-method)
* [aspect-oriented programming (AOP)](https://en.wikipedia.org/wiki/Aspect-oriented_programming#:~:text=In%20computing%2C%20aspect%2Doriented%20programming,separation%20of%20cross%2Dcutting%20concerns.&text=Aspect%2Doriented%20programming%20entails%20breaking,%2C%20cohesive%20areas%20of%20functionality)
* [SOLID](https://en.wikipedia.org/wiki/SOLID)
* spring-boot (2.2.4)