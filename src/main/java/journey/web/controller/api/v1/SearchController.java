package journey.web.controller.api.v1;

import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import journey.operation.Search;
import journey.web.dto.SearchDto;

import java.io.IOException;

@RequestMapping("/api/v1")
@RestController
@CrossOrigin
public class SearchController {
    @Autowired
    Search search;

    @GetMapping("/search/{collection}")
    public Object search(@RequestParam MultiValueMap<String, Object> params, @PathVariable String collection) throws IOException, SolrServerException {
        return new SearchDto(search.perform(params, collection)).getResult();
    }

    @GetMapping("/autocomplete/{collection}")
    public Object autocomplete(@RequestParam MultiValueMap<String, Object> params, @PathVariable String collection) throws IOException, SolrServerException {
        return new SearchDto(search.performAutocomplete(params, collection)).getResult();
    }
}

