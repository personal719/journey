package journey.web.controller.api.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import journey.aspect.annotation.Authorized;
import journey.aspect.annotation.CustomValidation;
import journey.aspect.annotation.UseCustomValidation;
import journey.operation.OwnershipService;
import journey.persistence.enitity.Ownership;
import journey.persistence.model.Scope;
import journey.web.factory.OwnershipFactory;

import java.io.IOException;

@RequestMapping("api/v1/ownership")
@RestController
public class OwnershipController {
    @Autowired
    OwnershipService ownershipService;

    @PostMapping(value = "/create")
    @UseCustomValidation
    @Authorized
    public Object create(Scope scope,
                         @CustomValidation(requiredClass = Ownership.class, converter = OwnershipFactory.class, converterSchemaValidation = "create")
                         @RequestParam("ownership") String schema,
                         @Value("${novalue:#{null}}") Ownership ownership
            , @RequestParam MultipartFile[] images) throws IOException {
        ownershipService.create(scope.getUser(), images, ownership);
        return null;
    }

    @PutMapping(value = "/edit")
    @UseCustomValidation
    @Authorized
    public Object update(Scope scope,
                         @CustomValidation(requiredClass = Ownership.class, converter = OwnershipFactory.class, converterSchemaValidation = "update")
                         @RequestParam("ownership") String schema,
                         @Value("${novalue:#{null}}") Ownership ownership
            , @RequestParam MultipartFile[] images) throws Throwable {
        ownershipService.update(scope.getUser(), images, ownership);
        return null;
    }

}
