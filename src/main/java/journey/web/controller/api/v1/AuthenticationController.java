package journey.web.controller.api.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import journey.aspect.annotation.Authorized;
import journey.aspect.annotation.CustomValidation;
import journey.aspect.annotation.UseCustomValidation;
import journey.web.dto.ChangePasswordDto;
import journey.operation.Authentication;
import journey.persistence.model.Scope;
import journey.util.JsonFormatter;
import journey.web.dto.*;

import static journey.persistence.enitity.Role.RoleName.ADMIN;

@RestController
@RequestMapping("/api/v1/auth")
public class AuthenticationController {
    @Autowired
    Authentication authentication;

    @PostMapping("/register")
    @UseCustomValidation
    public Object register(@RequestBody @CustomValidation(path = "/validation/register.json", requiredClass = RegisterDto.class) String schema, RegisterDto registerDto) {
        authentication.register(registerDto);
        return null;
    }

    @PostMapping("/login")
    @UseCustomValidation
    public Object login(@RequestBody @CustomValidation(path = "/validation/login.json", requiredClass = LoginDto.class) String schema, LoginDto loginDto) {
        return JsonFormatter.format("token", authentication.login(loginDto));
    }

    @PutMapping("/verify")
    @UseCustomValidation
    public Object verify(@RequestBody @CustomValidation(path = "/validation/verify.json", requiredClass = VerifyDto.class) String schema, VerifyDto verifyDto) {
        return JsonFormatter.format("token", authentication.verify(verifyDto));
    }

    @PostMapping("/forget-password")
    @UseCustomValidation
    public Object forgetPassword(@RequestBody @CustomValidation(path = "/validation/forget-password.json", requiredClass = ForgotPasswordDto.class) String schema,
                                 ForgotPasswordDto forgetPassword) {
        authentication.forgotPassword(forgetPassword);
        return null;
    }

    @PutMapping("/reset-password")
    @UseCustomValidation
    public Object resetPassword(@RequestBody @CustomValidation(path = "/validation/reset-password.json", requiredClass = ResetPasswordDto.class) String schema,
                                ResetPasswordDto resetPasswordDto) {
        return JsonFormatter.format("token", authentication.resetPassword(resetPasswordDto));
    }

    @PostMapping("/change-password")
    @UseCustomValidation
    @Authorized(roles = ADMIN)
    public Object changePassword(Scope scope, @RequestBody @CustomValidation(path = "/validation/change-password.json", requiredClass = ChangePasswordDto.class) String schema,
                                 ChangePasswordDto changePasswordDto) {
        authentication.changePassword(scope.getUser(), changePasswordDto);
        return null;
    }

}
