package journey.web.controller.api.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import journey.aspect.annotation.Unwrapped;
import journey.operation.MediaService;
import journey.persistence.enitity.Media;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@RestController
@RequestMapping("/media")
public class MediaController {
    @Autowired
    MediaService media;

    @Unwrapped
    @GetMapping("/images/{id}")
    public Object image(@PathVariable Long id) throws IOException {
        Media media = this.media.find(id);
        if (media == null)
            return null;
        File file = new File(media.getPath() + "/" + media.getName());
        Path path = Paths.get(file.getAbsolutePath());
        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));
        return ResponseEntity.ok()
                .contentLength(file.length()).
                        contentType(MediaType.IMAGE_JPEG).body(resource);
    }
}
