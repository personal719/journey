package journey.web.controller.api.v1;

import lombok.AllArgsConstructor;
import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import journey.aspect.annotation.Authorized;
import journey.aspect.annotation.Unwrapped;
import journey.operation.Document;
import journey.persistence.model.Scope;

import java.io.IOException;

import static journey.persistence.enitity.Role.RoleName.ADMIN;

@RestController
@AllArgsConstructor
@RequestMapping("api/v1")
public class AdminController {
    private final Document document;

    @DeleteMapping("/_truncate/collection/{collectionName}")
    @Authorized(roles = ADMIN)
    public Object truncateAllDocument(Scope scope, @PathVariable String collectionName) throws IOException, SolrServerException {
        document.truncate(collectionName);
        return null;
    }

    @DeleteMapping("/_delete/collection/{collectionName}/{id}")
    @Authorized(roles = ADMIN)
    public Object truncateAllDocument(Scope scope, @PathVariable String collectionName, @PathVariable String id) throws IOException, SolrServerException {
        document.delete(collectionName, id);
        return null;
    }

    @DeleteMapping("/_delete/cache/{key}")
    @Authorized(roles = ADMIN)
    @Unwrapped
    public Object deleteCacheKey(Scope scope, @PathVariable String key) {
        return null;
    }

    @DeleteMapping("/_delete/cache/all")
    @Authorized(roles = ADMIN)
    @Unwrapped
    public Object deleteAllCaches(Scope scope) {
        return null;
    }

}
