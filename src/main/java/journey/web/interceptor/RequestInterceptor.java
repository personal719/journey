package journey.web.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;
import journey.persistence.enitity.LogReq;
import journey.persistence.repository.LogReqRepo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

import static java.lang.String.format;
import static java.lang.String.join;

public class RequestInterceptor implements HandlerInterceptor {
    private final LogReqRepo logReqRepo;

    public RequestInterceptor(LogReqRepo logReqRepo) {
        this.logReqRepo = logReqRepo;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        request.setAttribute("started_at", System.currentTimeMillis());
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        LogReq log = new LogReq()
                .setCity(request.getHeader("city"))
                .setCountry(request.getHeader("country"))
                .setIpAddress(request.getHeader("ip"))
                .setToken(request.getHeader("token"))
                .setVisitorSession(request.getHeader("request_session"))
                .setUri(request.getRequestURI())
                .setStatus(response.getStatus())
                .setHost(request.getRemoteHost())
                .setException(ex == null ? null : ex.getMessage())
                .setProcessingPeriod(System.currentTimeMillis() - (Long.parseLong(request.getAttribute("started_at").toString())));
        setParams(request.getParameterMap(), log);
        logReqRepo.save(log);
    }

    private void setParams(Map<String, String[]> params, LogReq log) {
        StringBuilder paramsStr = new StringBuilder();
        params.forEach((key, value) -> paramsStr.append(format("%s=%s&&", key, join(",", value))));
        log.setParams(paramsStr.toString());
    }


}
