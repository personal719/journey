package journey.web.dto;

public class ChangePasswordDto {
    public String oldPassword;
    public String newPassword;
}
