package journey.web.dto;

public class ResetPasswordDto {
    public String password;
    public String code;
    public String phoneNumber;
}
