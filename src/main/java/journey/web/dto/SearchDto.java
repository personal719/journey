package journey.web.dto;

import lombok.Getter;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class SearchDto {
    @Getter
    private final Map<String, Object> result = new HashMap<>();
    private QueryResponse searchResponse;

    public SearchDto(QueryResponse response) {
        this.searchResponse = response;
        build();
        result.put("numFound", searchResponse.getResults().getNumFound());
    }

    private void build() {
        Map<String, List<Object>> response = new HashMap<>();
        Map<String, List<Object>> facets = new HashMap<>();
        for (SolrDocument entries : searchResponse.getResults()) {
            Map<String, Object> res = new HashMap<>();
            entries.forEach(res::put);
            response.computeIfAbsent("response", (k) -> new ArrayList<>()).add(res);
        }
        if (searchResponse.getFacetFields() != null)
            for (FacetField field : searchResponse.getFacetFields())
                facets
                        .computeIfAbsent(field.getName(), (k) -> new ArrayList<>())
                        .add(buildFacetField(field));

        result.put("result", response);
        result.put("facet", facets);
    }

    private Object buildFacetField(FacetField field) {
        return field.getValues().stream().map(this::facetDto).collect(Collectors.toList());
    }

    private Object facetDto(FacetField.Count facet) {
        Map<String, Object> count = new HashMap<>();
        count.put("name", facet.getName());
        count.put("count", facet.getCount());
        return count;
    }

}
