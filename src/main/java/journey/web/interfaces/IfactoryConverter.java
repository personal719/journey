package journey.web.interfaces;

import com.fasterxml.jackson.core.JsonProcessingException;
import journey.persistence.enitity.Ownership;

public interface IfactoryConverter<T> {
    T getObject(String body, String type) throws JsonProcessingException;

    String validationSchema(Class<? extends Ownership> ownershipClass, String schemaKey);

    Class<? extends Ownership> getType(String type);
}
