package journey.web.config;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import journey.persistence.repository.LogReqRepo;
import journey.web.interceptor.RequestInterceptor;

@Component
public class WebInterceptor implements WebMvcConfigurer {
    private final LogReqRepo logReqRepo;

    public WebInterceptor(LogReqRepo logReqRepo) {
        this.logReqRepo = logReqRepo;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new RequestInterceptor(logReqRepo));
    }
}
