package journey.web.factory;

import com.fasterxml.jackson.core.JsonProcessingException;
import journey.persistence.enitity.Ownership;
import journey.web.interfaces.IfactoryConverter;

public class NullConverter implements IfactoryConverter {

    @Override
    public Object getObject(String body, String type) throws JsonProcessingException {
        return null;
    }

    @Override
    public Class<? extends Ownership> getType(String type) {
        return null;
    }

    @Override
    public String validationSchema(Class ownershipClass, String schemaKey) {
        return null;
    }
}
