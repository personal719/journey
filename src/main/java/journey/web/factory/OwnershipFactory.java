package journey.web.factory;

import com.fasterxml.jackson.core.JsonProcessingException;
import javafx.util.Pair;
import org.omg.CORBA.INITIALIZE;
import journey.persistence.enitity.Ownership;
import journey.util.JsonUtil;
import journey.web.interfaces.IfactoryConverter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OwnershipFactory implements IfactoryConverter {
    private static final Map<String, Class<? extends Ownership>> ownershipFactory = new HashMap<>();
    private static final Map<Class<? extends Ownership>, List<Pair<String, String>>> validationSchema = new HashMap<>();

    @Override
    public Object getObject(String body, String type) throws JsonProcessingException {
        return JsonUtil.fromJson(body, ownershipFactory.get(type));
    }

    @Override
    public String validationSchema(Class ownershipClass, String schemaKey) {
        return validationSchema
                .get(ownershipClass)
                .stream()
                .filter(f -> f.getKey().equals(schemaKey))
                .findFirst().orElseThrow(INITIALIZE::new).getValue();
    }

    @Override
    public Class getType(String type) {
        return ownershipFactory.get(type);
    }

    public static void addType(String name, Class<? extends Ownership> ownershipClass, Pair<String, String> validPair) {
        ownershipFactory.put(name, ownershipClass);
        validationSchema.computeIfAbsent(ownershipClass, (e) -> new ArrayList<>()).add(validPair);
    }

}
