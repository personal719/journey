package journey.web.container;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import org.apache.commons.configuration2.*;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import journey.config.OwnershipSetting;
import journey.util.GlobalParams;

import java.util.Arrays;
import java.util.Optional;

@Component
public class Infrastructure {
    @Bean
    PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public CompositeConfiguration configuration() {
        CompositeConfiguration compositeConfiguration = new CompositeConfiguration();
        compositeConfiguration.addConfiguration(new EnvironmentConfiguration());
        return compositeConfiguration;
    }

    @Bean
    public GlobalParams globalParams(CompositeConfiguration compositeConfiguration) {
        GlobalParams globalParams = new GlobalParams(compositeConfiguration);
        globalParams
                .addFile("env.properties")
                .addFile("ownership.properties");
        return globalParams;
    }

    @Bean
    public Algorithm jwtAlgorithm(GlobalParams params) {
        return Algorithm.HMAC256(params.get("jwt_secret"));
    }

    @Bean
    public JWTVerifier jwtVerifier(GlobalParams params, Algorithm algorithm) {
        return JWT.require(algorithm)
                .withIssuer(params.get("issuer"))
                .build();
    }

    @Bean
    public SolrClient solrClient(GlobalParams params) {
        CloudSolrClient.Builder builder = new CloudSolrClient
                .Builder(Arrays.asList(params.get("ZKHOST")
                .split(",")), Optional.empty());
        return builder.build();
    }

    @Bean
    public OwnershipSetting ownershipSetting(GlobalParams params) {
        return OwnershipSetting.getInstance(params);
    }

}
