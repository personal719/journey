package journey.exception;

public class InitialError extends RuntimeException {

    public InitialError(String msg) {
        super(msg);
    }
}
