package journey.exception;

public class UnAuthorized extends RuntimeException {
    public UnAuthorized() {
        super("UnAuthorized");
    }

    public UnAuthorized(String msg) {
        super(msg);
    }
}
