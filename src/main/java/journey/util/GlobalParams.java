package journey.util;

import org.apache.commons.configuration2.*;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.ex.ConfigurationException;

public class GlobalParams {

    private final CompositeConfiguration config;

    public GlobalParams(CompositeConfiguration config) {
        this.config = config;
    }

    public String get(String key) {
        return config.getString(key);
    }

    public int getInt(String key) {
        return Integer.valueOf(get(key));
    }

    public GlobalParams addFile(String pathname) {
        Configuration fileConfig = readConfigFile(pathname);
        if (fileConfig != null)
            config.addConfiguration(fileConfig);
        return this;
    }

    private Configuration readConfigFile(String pathname) {
        try {
            Parameters params = new Parameters();

            return new FileBasedConfigurationBuilder<FileBasedConfiguration>(PropertiesConfiguration.class)
                    .configure(params.fileBased()
                            .setFileName(pathname)).
                            getConfiguration();
        } catch (ConfigurationException ignored) {
            return null;
        }
    }
}
