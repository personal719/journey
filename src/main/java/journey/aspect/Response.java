package journey.aspect;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Response {
    private String message;
    private boolean success;
    private Integer code;
    private Long time;
    private Object data;
    private Object errors;
}
