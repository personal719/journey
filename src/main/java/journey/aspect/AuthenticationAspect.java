package journey.aspect;


import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import journey.aspect.annotation.Authorized;
import journey.exception.UnAuthorized;
import journey.operation.Authentication;
import journey.persistence.crud.UserCrud;
import journey.persistence.model.Scope;

import javax.servlet.http.HttpServletRequest;

@Component
@Aspect
public class AuthenticationAspect {
    @Autowired
    UserCrud userCrud;
    @Autowired
    Authentication authentication;

    @Pointcut("execution(* journey.web.controller..*.*(..))")
    public void atExecution() {
    }

    @Pointcut("@annotation(annotation)")
    public void authorized(Authorized annotation) {
    }

    @Pointcut("args(scope,..)")
    public void scoped(Scope scope) {
    }

    @Before(" scoped(scope) && authorized(annotation)")
    public void authenticate(Scope scope, Authorized annotation) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String token = request.getHeader("Authorization");
        if (token == null) throw new UnAuthorized("Authorization is required");
        authentication.authenticate(scope, token);
    }

    @Before("scoped(scope) && authorized(annotation)")
    public void authorize(Scope scope, Authorized annotation) {
        if (!authentication.hasAuthorized(scope, annotation))
            throw new UnAuthorized("Permission Denied");
    }

}
