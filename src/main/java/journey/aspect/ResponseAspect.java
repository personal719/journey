package journey.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.everit.json.schema.ValidationException;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import journey.exception.InvalidVerify;
import journey.exception.NotFoundException;
import journey.exception.ResourceAlreadyExist;
import journey.exception.UnAuthorized;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Component
@Aspect
@Order(0)
public class ResponseAspect {
    @Pointcut("execution(* journey.web.controller..*.*(..))")
    public void atExecution() {
    }

    @Pointcut("@annotation(journey.aspect.annotation.Unwrapped)")
    public void unWrapped() {
    }

    @Around("atExecution() && !unWrapped()")
    public Object response(ProceedingJoinPoint jp) throws Throwable {
        long startTime = System.currentTimeMillis();
        Response.ResponseBuilder builderResponse = Response.builder();
        try {
            builderResponse
                    .data(jp.proceed())
                    .code(HttpStatus.OK.value())
                    .success(true);
        } catch (UnAuthorized e) {
            builderResponse
                    .message(e.getMessage())
                    .code(HttpStatus.UNAUTHORIZED.value());
        } catch (ResourceAlreadyExist e) {
            builderResponse
                    .message(e.getMessage())
                    .code(HttpStatus.CONFLICT.value());
        } catch (InvalidVerify e) {
            builderResponse
                    .message(e.getMessage())
                    .code(HttpStatus.BAD_REQUEST.value());
        } catch (NotFoundException e) {
            builderResponse
                    .message(e.getMessage())
                    .code(HttpStatus.NOT_FOUND.value());
        } catch (ValidationException e) {
            HashMap<String, List<String>> message = new HashMap<>();
            e.getCausingExceptions()
                    .forEach(f -> message.computeIfAbsent(f.getPointerToViolation(), (v) -> new ArrayList<>())
                            .add(f.getMessage()));
            builderResponse
                    .message(e.getCausingExceptions().size() == 0 ? e.getMessage() : null)
                    .code(HttpStatus.BAD_REQUEST.value())
                    .errors(message);
        } catch (RuntimeException e) {
            builderResponse
                    .message(e.getMessage())
                    .code(HttpStatus.SERVICE_UNAVAILABLE.value());
            e.printStackTrace();
        }
        Response response = builderResponse
                .time(calcTime(startTime))
                .build();
        return ResponseEntity
                .status(response.getCode())
                .body(response);
    }

    private Long calcTime(long startTime) {
        return System.currentTimeMillis() - startTime;
    }

}
