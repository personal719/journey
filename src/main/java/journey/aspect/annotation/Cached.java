package journey.aspect.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Cached {
    String key() default "";

    long maximumSize() default 25;

    long expireAfterWrite() default 1;

    TimeUnit timeUnit() default TimeUnit.MINUTES;
}
