package journey.aspect.annotation;

import journey.web.factory.NullConverter;
import journey.web.interfaces.IfactoryConverter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER})
public @interface CustomValidation {
    String path() default "";

    Class<?> requiredClass();

    Class<? extends IfactoryConverter> converter() default NullConverter.class;

    String converterSchemaValidation() default "";
}

