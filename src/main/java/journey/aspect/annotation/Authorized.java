package journey.aspect.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static journey.persistence.enitity.Role.RoleName.USER;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Authorized {
    String[] roles() default USER;

    Operation operation() default Operation.And;

    public enum Operation {
        And,
        Or
    }
}
