package journey.aspect.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Batched {
    int size() default 2;

    String methodName() default "saveAll";

    long time() default 1;

    TimeUnit timeUnit() default TimeUnit.MINUTES;

    Class argumentType() default Collection.class;

}

