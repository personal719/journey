package journey.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import journey.Application;
import journey.aspect.annotation.CustomValidation;
import journey.util.JsonUtil;
import journey.web.interfaces.IfactoryConverter;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashMap;

@Aspect
@Component
@Order(2)
public class ValidationAspect {

    @Around("@annotation(journey.aspect.annotation.UseCustomValidation)")
    public Object validateAdvice(ProceedingJoinPoint jp) throws Throwable {
        MethodSignature methodSignature = (MethodSignature) jp.getSignature();
        Class<?> clazz = methodSignature.getDeclaringType();
        Method method = clazz.getDeclaredMethod(methodSignature.getName(), methodSignature.getParameterTypes());
        int numArgs = 0;
        HashMap<Class, Integer> map = new HashMap<>();
        for (int i = 0; i < jp.getArgs().length; i++)
            map.put(method.getParameterTypes()[i], i);

        for (Annotation[] annotations : method.getParameterAnnotations()) {
            for (Annotation annotation : annotations) {

                if (CustomValidation.class.isInstance(annotation)) {
                    CustomValidation customValidation = (CustomValidation) annotation;
                    IfactoryConverter factoryConverter = null;

                    if (customValidation.path().isEmpty())
                        factoryConverter = initiateConverter(customValidation.converter());
                    try {
                        String validationPath = null;
                        Schema schema;
                        String arg = (String) jp.getArgs()[numArgs];
                        JSONObject subject = new JSONObject(arg);

                        if (factoryConverter != null)
                            validationPath = factoryConverter.validationSchema(factoryConverter.getType((String) subject.get("type")), customValidation.converterSchemaValidation());
                        else validationPath = customValidation.path();

                        System.out.println("Validation Path" + validationPath);
                        schema = SchemaLoader.load(new JSONObject(new JSONTokener(Application.class.getResourceAsStream(validationPath))));

                        schema.validate(subject);
                        jp.getArgs()[map.get(customValidation.requiredClass())] = factoryConverter == null ? JsonUtil.fromJson((String) jp.getArgs()[numArgs], customValidation.requiredClass()) : factoryConverter.getObject(arg, (String) subject.get("type"));
                        return jp.proceed(jp.getArgs());
                    } catch (ValidationException e) {
                        throw e;
                    } catch (JSONException js) {
                        js.printStackTrace();
                        return "type is required";
                    }
                }
            }
            numArgs++;
        }
        return jp.proceed();
    }

    private IfactoryConverter initiateConverter(Class<? extends IfactoryConverter> converter) throws IllegalAccessException, InstantiationException {
        return converter.newInstance();
    }


}


