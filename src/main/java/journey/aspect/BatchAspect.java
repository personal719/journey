package journey.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import journey.aspect.annotation.Batched;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

import static java.lang.String.format;

@Aspect
@Component
@Slf4j
public class BatchAspect {
    private final ConcurrentMap<String, Batch> batches = new ConcurrentHashMap<>();

    BatchAspect() {
        Runtime.getRuntime()
                .addShutdownHook(new Thread(() ->
                        batches.forEach((k, batch) -> batch.flush())));
    }

    @Pointcut("@annotation(batched)")
    public void batched(Batched batched) {
    }

    @Around("batched(batch) && args(arg) ")
    public void insertBatch(ProceedingJoinPoint jp, Batched batch, Object arg) {
        String key = jp.getSignature().toString();
        batches.computeIfAbsent(key, (k) -> new Batch(batch, jp.getTarget()))
                .add(arg);
    }

    public class Batch {
        private final ConcurrentLinkedQueue args = new ConcurrentLinkedQueue();
        private final Batched batched;
        private final Object target;
        private final ScheduledExecutorService pool = Executors.newScheduledThreadPool(4);

        public Batch(Batched batched, Object target) {
            this.batched = batched;
            this.target = target;
            pool.scheduleWithFixedDelay(this::flush, batched.time(), batched.time(), batched.timeUnit());
        }

        public void add(Object o) {
            args.add(o);
            if (batched.size() <= args.size())
                flush();
        }

        public void flush() {

            pool.submit(() -> insertBatch(copyAndClear()));

        }

        private void insertBatch(List copy) {
            long start = System.currentTimeMillis();
            try {
                Method method = target.getClass().getMethod(batched.methodName(), batched.argumentType());
                method.invoke(target, copy);
                if (copy.size() != 0)
                    log.info(format("batch insertion successfully time{} %s", System.currentTimeMillis() - start));

            } catch (NoSuchMethodException e) {
                throw new RuntimeException(
                        format("%s %s %s", target.getClass().getName(), " must have a method called: ", batched.methodName()));
            } catch (IllegalAccessException | InvocationTargetException e) {
                log.error("Batch insertion error", e);
                throw new RuntimeException(e);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Message " + e.getMessage());
                log.error("Batch insertion error", e);
                args.addAll(copy);
            }
        }

        private List copyAndClear() {
            ArrayList copy;
            synchronized (args) {
                copy = new ArrayList<>(args);
                args.clear();
            }
            return copy;
        }
    }
}

