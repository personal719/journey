package journey.aspect;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import journey.aspect.annotation.Cached;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Aspect
@Component
public class CacheAspect {
    private ConcurrentMap<String, Cache<String, Object>> caches = new ConcurrentHashMap<>();

    @Pointcut("@annotation(cached)")
    public void cachedAnnotation(Cached cached) {
    }

    @Around("cachedAnnotation(cached)")
    public Object cache(ProceedingJoinPoint jp, Cached cached) {
        String cachesKey = cached.key().isEmpty() ? jp.getSignature().toString() : cached.key();
        String methodKey = compteKey(jp);
        Cache cache = getMethodCache(cachesKey, cached);
        Object result = cache.getIfPresent(methodKey);
        if (result != null) return result;
        try {
            result = jp.proceed();
            cache.put(methodKey, result);
        } catch (RuntimeException e) {
            throw e;
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
        return result;
    }

    String compteKey(ProceedingJoinPoint jp) {
        StringBuilder builder = new StringBuilder();
        for (Object o : jp.getArgs())
            builder.append(o.toString())
                    .append("&&");
        return builder.toString();
    }

    private Cache getMethodCache(String cachesKey, Cached cached) {
        return caches.computeIfAbsent(cachesKey, k -> Caffeine.newBuilder()
                .maximumSize(cached.maximumSize())
                .expireAfterWrite(cached.expireAfterWrite(), cached.timeUnit())
                .recordStats().build());
    }

    @Around("execution(*  journey.web.controller.api.v1.AdminController.deleteAllCaches(..))")
    public Object invalidateAll(ProceedingJoinPoint jp) {
        caches.forEach((k, v) -> v.invalidateAll());
        return null;
    }


    @Around("execution(* journey.web.controller.api.v1.AdminController.deleteCacheKey(..,key))")
    public Object invalidateKey(ProceedingJoinPoint jp, String key) {
        caches.get(key).invalidateAll();
        return null;
    }
}
