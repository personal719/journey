package journey.persistence.repository;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;
import journey.aspect.annotation.Batched;
import journey.persistence.crud.LogReqCrud;
import journey.persistence.enitity.LogReq;

import java.util.Collection;

@Repository
@AllArgsConstructor
public class LogReqRepo {
    private final LogReqCrud crud;

    @Batched
    public void save(LogReq log) {
        crud.save(log);
    }

    public void saveAll(Collection<LogReq> logs) {
        crud.saveAll(logs);
    }

}
