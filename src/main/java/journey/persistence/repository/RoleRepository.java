package journey.persistence.repository;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;
import journey.aspect.annotation.Cached;
import journey.persistence.crud.RoleCrud;
import journey.persistence.enitity.Role;

import java.util.concurrent.TimeUnit;

@Repository
@AllArgsConstructor
public class RoleRepository {
    private final RoleCrud crud;

    @Cached(timeUnit = TimeUnit.HOURS, expireAfterWrite = 10, key = "roles")
    public Iterable<Role> findAll() {
        return crud.findAll();
    }
}
