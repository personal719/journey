package journey.persistence.model;

import lombok.Data;

import java.util.Map;

@Data
public class SearchResult {
    private int numResult;
    private String query;
    private Map<String, Object> result;
}
