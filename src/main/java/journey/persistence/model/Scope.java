package journey.persistence.model;

import lombok.Data;
import journey.persistence.enitity.User;

@Data
public class Scope {
    private User user;
}
