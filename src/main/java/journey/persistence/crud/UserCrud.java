package journey.persistence.crud;

import org.springframework.data.repository.CrudRepository;
import journey.persistence.enitity.User;

import java.util.Optional;

public interface UserCrud extends CrudRepository<User, Long> {
    Optional<User> findByPhoneNumberAndVerified(String phoneNumber, Boolean verified);

    Optional<User> findByPhoneNumber(String phoneNumber);
}
