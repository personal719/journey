package journey.persistence.crud;

import org.springframework.data.repository.CrudRepository;
import journey.persistence.enitity.Media;

import java.util.List;

public interface MediaCrud extends CrudRepository<Media, Long> {

    List<Media> findByModelIdAndModelNameAndTableName(Long modelId, String modelName, String tableName);

}
