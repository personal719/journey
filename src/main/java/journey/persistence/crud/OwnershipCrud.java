package journey.persistence.crud;

import org.springframework.data.repository.CrudRepository;
import journey.persistence.enitity.Ownership;

public interface OwnershipCrud<T extends Ownership> extends CrudRepository<T, Long> {

}
