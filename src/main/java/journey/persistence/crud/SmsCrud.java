package journey.persistence.crud;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import journey.persistence.enitity.Sms;

import java.time.LocalDateTime;
import java.util.Optional;

public interface SmsCrud extends JpaRepository<Sms, Long> {

    Optional<Sms> findFirstByPhoneNumberAndCodeOrderByCreatedAtDesc(@Param("phoneNumber") String phoneNumber,
                                                                    @Param("code") String code);

    Optional<Sms> findFirstByPhoneNumberAndCodeAndCreatedAtGreaterThanEqualOrderByCreatedAtDesc(String phoneNumber, String code, LocalDateTime createdAt);


}
