package journey.persistence.crud;

import org.springframework.data.repository.CrudRepository;
import journey.persistence.enitity.Role;

public interface RoleCrud extends CrudRepository<Role, Long> {
}
