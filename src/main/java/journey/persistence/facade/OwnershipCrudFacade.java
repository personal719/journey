package journey.persistence.facade;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.stereotype.Repository;
import journey.persistence.crud.OwnershipCrud;
import journey.persistence.enitity.Chalet;
import journey.persistence.enitity.Farm;
import journey.persistence.enitity.Journey;
import journey.persistence.enitity.Ownership;

@AllArgsConstructor
@Repository
public class OwnershipCrudFacade {
    @Getter
    private final OwnershipCrud<Ownership> ownershipCrud;
    @Getter
    private final OwnershipCrud<Farm> farmCrud;
    @Getter
    private final OwnershipCrud<Chalet> chaletCrud;
    @Getter
    private final OwnershipCrud<Journey> journeyCrud;
}
