package journey.persistence.enitity;

import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Accessors(chain = true)
public class Sms {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(referencedColumnName = "id", columnDefinition = "user_id")
    private User user;
    private String phoneNumber;
    private String code;
    private String visitorSession;
    private boolean isSent;
    private boolean isUsed;
    @Convert(converter = CreatedTypeConverter.class)
    private CreatedType createdFor;

    @CreationTimestamp
    private LocalDateTime createdAt;

    @UpdateTimestamp
    private LocalDateTime updatedAt;

    public enum CreatedType {
        ForgetPassword("forget-password"),

        VerifyUser("verify-user");

        public final String used;

        CreatedType(String used) {
            this.used = used;
        }
    }
}
