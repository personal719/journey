package journey.persistence.enitity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import journey.operation.FullPayLoad;
import journey.operation.PayLoadObjectSolr;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Accessors(chain = true)
@PrimaryKeyJoinColumn(name = "ownership_id", referencedColumnName = "id")
public class Farm extends Ownership implements FullPayLoad {
    private Boolean hasPool;
    private Integer depthPool;
    private Boolean hasCourt;

    @Override
    public String getTableName() {
        return "farm";
    }

    @Override
    public PayLoadObjectSolr payload() {
        return super.payload()
                .put("has_pool_b", hasPool)
                .put("depth_pool_b", depthPool)
                .put("has_court_b", hasPool);
    }

    @Override
    public List<PayLoadObjectSolr> payloadAutoComplete() {
        return super.payloadAutoComplete();
    }

    @Override
    protected Object getArabicTableName() {
        return "مزرعة";
    }
}
