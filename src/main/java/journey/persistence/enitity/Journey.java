package journey.persistence.enitity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import java.util.Date;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@PrimaryKeyJoinColumn(name = "ownership_id", referencedColumnName = "id")
@Accessors(chain = true)
public class Journey extends Ownership {

    private Date startedAt;
    private Date endAt;
    private String fromRegion;
    private String toRegion;

    @Override
    public String getTableName() {
        return "Joruney";
    }

    @Override
    protected Object getArabicTableName() {
        return "رحلة";
    }

}
