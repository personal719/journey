package journey.persistence.enitity;

import lombok.Data;
import lombok.experimental.Accessors;
import journey.config.OwnershipSetting;
import journey.operation.FullPayLoad;
import journey.operation.PayLoadObjectSolr;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static journey.operation.SolrType.SolrTypeField.*;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Data
@Accessors(chain = true)
public abstract class Ownership implements FullPayLoad {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String type;
    private String description;
    private Boolean active;
    private String phoneNumber;
    private String governate;
    private String region;
    private Long userId;
    @Transient
    private List<Media> media = new ArrayList<>();

    @Transient
    public abstract String getTableName();

    @Override
    public PayLoadObjectSolr payload() {
        return new PayLoadObjectSolr()
                .put("id", id)
                .put("name_t_bi", name)
                .put("description_t_bi", description)
                .put("active_b", active)
                .put("phoneNumber_s", phoneNumber)
                .put("user_id_i", userId)
                .put("category_s", getArabicTableName())
                .put("regions_s", region)
                .put("governate_s", governate)
                .put("images_sa", media
                        .stream()
                        .map(Media::getUrl)
                        .collect(Collectors.toList()));
    }

    @Override
    public List<PayLoadObjectSolr> payloadAutoComplete() {
        ArrayList<PayLoadObjectSolr> payloads = new ArrayList<>();
        payloads.add(payloadName());
        payloads.add(payLoadGovernate());
        payloads.add(payLoadRegion());
        return payloads;
    }

    private PayLoadObjectSolr payloadName() {
        return new PayLoadObjectSolr()
                .put("id", format("%s-%s-%s", "name", getTableName(), id))
                .put("phrase", name)
                .put("description_s", description)
                .put("category_s", getArabicTableName())
                .put("active_b", active)
                .put("region_s", region)
                .put("governate_s", governate)
                .put("user_id_i", userId)
                .put("type_s", NAVIGATIONAL.toString())
                .put("image_s", (media.size() != 0) ? media.get(0).getUrl() : null);
    }

    protected abstract Object getArabicTableName();

    private PayLoadObjectSolr payLoadRegion() {
        return new PayLoadObjectSolr()
                .put("id", format("%s %s", region, OwnershipSetting.getInstance().translateAsArPlural(getTableName())))
                .put("phrase", format("%s %s", region, OwnershipSetting.getInstance().translateAsArPlural(getTableName())))
                .put("table_s", getTableName())
                .put("type_s", FILTER.toString())
                .put("filter_sa", format("fq=region_s:%s&&fq=category_s:%s", region, getArabicTableName()))
                .put("count_i", INCREMENT);
    }

    private PayLoadObjectSolr payLoadGovernate() {
        return new PayLoadObjectSolr()
                .put("id", format("%s %s", governate, OwnershipSetting.getInstance().translateAsArPlural(getTableName())))
                .put("phrase", format("%s %s", governate, OwnershipSetting.getInstance().translateAsArPlural(getTableName())))
                .put("table_s", getTableName())
                .put("type_s", FILTER.toString())
                .put("filter_sa", format("fq=governate_s:%s&&fq=category_s:%s", governate, getArabicTableName()))
                .put("count_i", INCREMENT);
    }
}