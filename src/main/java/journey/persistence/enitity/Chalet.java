package journey.persistence.enitity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import journey.operation.PayLoadObjectSolr;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;


@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Accessors(chain = true)
@PrimaryKeyJoinColumn(name = "ownership_id", referencedColumnName = "id")
public class Chalet extends Ownership {
    private String chaletState;

    @Override
    public String getTableName() {
        return "chalet";
    }

    @Override
    protected Object getArabicTableName() {
        return "شاليه";
    }

    @Override
    public PayLoadObjectSolr payload() {
        return super.payload()
                .put("chalet_state_s", chaletState);
    }
}
