package journey.persistence.enitity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private Long priority;

    public class RoleName {
        public final static String ADMIN = "ADMIN";
        public final static String USER = "USER";
    }
}
