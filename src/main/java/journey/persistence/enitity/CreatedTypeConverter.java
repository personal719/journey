package journey.persistence.enitity;

import org.omg.CORBA.INITIALIZE;

import javax.persistence.AttributeConverter;
import java.util.Arrays;

public class CreatedTypeConverter implements AttributeConverter<Sms.CreatedType, String> {
    @Override
    public String convertToDatabaseColumn(Sms.CreatedType createdType) {
        return createdType == null ? null : createdType.used;
    }

    @Override
    public Sms.CreatedType convertToEntityAttribute(String s) {
        return s == null ? null : Arrays.stream(Sms.CreatedType.values())
                .filter(f -> f.used.equals(s))
                .findFirst()
                .orElseThrow(INITIALIZE::new);
    }
}
