package journey.operation;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import journey.exception.NotFoundException;
import journey.exception.UnAuthorized;
import journey.persistence.enitity.Media;
import journey.persistence.enitity.Ownership;
import journey.persistence.enitity.User;
import journey.persistence.facade.OwnershipCrudFacade;
import journey.util.GlobalParams;

import java.io.IOException;
import java.util.List;

import static java.lang.String.format;

@Service
public class OwnershipService {
    private final MediaService mediaService;
    private final OwnershipCrudFacade ownershipFacade;
    private final Document document;
    private final String collectionName;

    public OwnershipService(MediaService mediaService, OwnershipCrudFacade ownershipFacade, Document document, GlobalParams params) {
        this.mediaService = mediaService;
        this.ownershipFacade = ownershipFacade;
        this.document = document;
        this.collectionName = params.get("ownership_collection");
    }

    @Transactional
    public void create(User user, MultipartFile[] images, Ownership ownership) throws IOException {
        ownership.setUserId(user.getId());
        ownershipFacade.getOwnershipCrud().save(ownership);
        List<Media> media = mediaService.storeImages(images, ownership.getClass().getName(), ownership.getId(), ownership.getTableName());
        ownership.setMedia(media);
        document.indexing(ownership, collectionName);
    }

    @Transactional
    public void update(User user, MultipartFile[] images, Ownership ownership) throws IOException {
        Ownership ship = ownershipFacade.getOwnershipCrud().findById(ownership.getId())
                .orElseThrow(() -> new NotFoundException(format("Ownership %s Not exists", ownership.getId())));
        if (!user.getId().equals(ship.getUserId()))
            throw new UnAuthorized("You haven't authorize to edit this object");
        ownership.setUserId(user.getId());
        ownershipFacade.getOwnershipCrud().save(ownership);
        List<Media> media = mediaService.updateImages(images, ownership.getClass().getName(), ownership.getId(), ownership.getTableName());
        ownership.setMedia(media);
        document.indexing(ownership, collectionName);
    }

}




