package journey.operation;

import journey.exception.NotFoundException;

import static java.lang.String.format;

public class SolrUtil {
    public static final String AUTOCOMPLETE_SUFFIX = "-autocomplete";

    public static String generateAutoCompleteId(Long id, String fieldName, String value, SolrType.SolrTypeField solrTypeField) {
        return generateAutoCompleteId(String.valueOf(id), fieldName, value, solrTypeField);
    }

    public static String generateAutoCompleteId(String id, String fieldName, String value, SolrType.SolrTypeField solrTypeField) {
        switch (solrTypeField) {
            case NAVIGATIONAL:
                return format("%s-%s-%s", id, fieldName, value);
            case FILTER:
                return format("%s-%s", fieldName, value);
        }
        throw new NotFoundException();
    }

    public static String getAutocompleteCollection(String collectionName) {
        return format("%s-autocomplete", collectionName);
    }
}
