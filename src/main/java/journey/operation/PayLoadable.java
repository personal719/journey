package journey.operation;

public interface PayLoadable {
    PayLoadObjectSolr payload();
}
