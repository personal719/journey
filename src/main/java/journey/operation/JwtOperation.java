package journey.operation;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import journey.exception.InitialError;
import journey.util.GlobalParams;

@Service
@AllArgsConstructor
public class JwtOperation {
    private final JWTVerifier jwtVerifier;
    private final GlobalParams params;

    DecodedJWT verify(String token) {
        return jwtVerifier.verify(token);
    }

    JWTCreator.Builder getBuilder() {
        if (params.get("issuer") != null)
            return JWT.create()
                    .withIssuer(params.get("issuer"));
        throw new InitialError("configuration [issuer] required");
    }
}
