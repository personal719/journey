package journey.operation;

import lombok.extern.slf4j.Slf4j;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import journey.aspect.annotation.Cached;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static java.lang.Integer.parseInt;
import static java.lang.String.format;
import static java.lang.String.join;
import static journey.operation.SolrUtil.getAutocompleteCollection;

@Service
@Slf4j
public class Search {
    private final SolrClient solrClient;
    private final static Map<String, String[]> defaultVariables = new HashMap<>();

    public Search(SolrClient solrClient) {
        this.solrClient = solrClient;
        initDefaultVariables();
    }

    private void initDefaultVariables() {
        defaultVariables.put("post", new String[]{"name_t_bi", "governate_s", "category_s", "regions_s"});
    }

    @Cached
    public QueryResponse perform(MultiValueMap<String, Object> params, String collection) throws IOException, SolrServerException {
        SolrQuery query = decorateSearchQuery(params, collection);
        log.debug("[Query] {}/select?{}", query);
        return solrClient.query(collection, query);
    }

    @Cached
    public QueryResponse performAutocomplete(MultiValueMap<String, Object> params, String collection) throws IOException, SolrServerException {
        SolrQuery query = decorateAutocompleteQuery(params);
        return solrClient.query(getAutocompleteCollection(collection), query);
    }

    private SolrQuery decorateSearchQuery(MultiValueMap<String, Object> params, String collection) {
        String q = getQ(params);
        SolrQuery query = new SolrQuery(q);
        buildDefaultFields(query, collection);
        buildQuery(params, query);
        return query;
    }

    private SolrQuery decorateAutocompleteQuery(MultiValueMap<String, Object> params) {
        String q = getQ(params);
        String phraseQuery = format("phrase:%s", q);
        String phraseGramQuery = format("phrase_gram:%s", q);
        String phraseEdgeQuery = format("phrase_edge:%s", q);
        String booleanQuery = format("{!bool must=%s should=%s should=%s}", phraseGramQuery, phraseEdgeQuery, phraseQuery);
        SolrQuery query = new SolrQuery(booleanQuery);
        buildQuery(params, query);
        return query;
    }

    private String getQ(MultiValueMap<String, Object> params) {
        return (params.get("q") == null || String.valueOf(params.getFirst("q")).isEmpty()) ? "*" : (String) params.getFirst("q");
    }

    private void buildQuery(MultiValueMap<String, Object> params, SolrQuery query) {
        buildFilter(params, query);
        buildFacet(params, query);
        buildFields(params, query);
        buildPaginates(params, query);
        log.debug("[Query] {}/select?{}", query);
    }

    private void buildFilter(MultiValueMap<String, Object> params, SolrQuery query) {
        if (params.get("fq") != null)
            params.get("fq")
                    .forEach(f -> query.addFilterQuery((String) f));
    }

    private void buildFacet(MultiValueMap<String, Object> params, SolrQuery query) {
        if (params.get("facet") != null)
            params.get("facet")
                    .forEach(f -> query.addFacetField((String) f));
        query.setFacetMinCount(1);
    }

    private void buildFields(MultiValueMap<String, Object> params, SolrQuery query) {
        if (params.get("fl") != null)
            params.get("fl")
                    .forEach(f -> query.addField((String) f));
    }

    private void buildDefaultFields(SolrQuery query, String collection) {
        query.set("defType", "edismax");
        query.set("qf", join(" ", defaultVariables.get(collection)));
    }

    private void buildPaginates(MultiValueMap<String, Object> params, SolrQuery query) {
        query.setRows(params.get("rows") == null ? 10 : parseInt((String) params.getFirst("rows")));
        query.setStart(params.get("start") == null ? 0 : parseInt((String) params.getFirst("start")));
    }

}
