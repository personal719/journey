package journey.operation;

import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import journey.aspect.annotation.Authorized;
import journey.aspect.annotation.Cached;
import journey.exception.*;
import journey.persistence.crud.SmsCrud;
import journey.persistence.crud.UserCrud;
import journey.persistence.enitity.Role;
import journey.persistence.enitity.Sms;
import journey.persistence.enitity.User;
import journey.persistence.model.Scope;
import journey.persistence.repository.RoleRepository;
import journey.web.dto.ChangePasswordDto;
import journey.web.dto.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@AllArgsConstructor
public class Authentication {
    private final JwtOperation jwt;
    private final PasswordEncoder passwordEncoder;
    private final Algorithm algorithm;
    private final UserCrud userCrud;
    private final SmsCrud smsCrud;
    private final RoleRepository roleRepo;

    public void register(RegisterDto registerDto) {
        Optional<User> optionalUser = userCrud.findByPhoneNumberAndVerified(registerDto.phoneNumber, true);
        if (optionalUser.isPresent())
            throw new ResourceAlreadyExist();
        User user = new User()
                .setUsername(registerDto.userName)
                .setPassword(passwordEncoder.encode(registerDto.password))
                .setPhoneNumber(registerDto.phoneNumber);
        userCrud.save(user);
        smsCrud.save(generateSms(user, Sms.CreatedType.VerifyUser));
    }

    public String login(LoginDto loginDto) {
        User user = userCrud.findByPhoneNumberAndVerified(loginDto.phoneNumber, true)
                .orElseThrow(UnAuthorized::new);
        if (!passwordEncoder.matches(loginDto.password, user.getPassword()))
            throw new UnAuthorized();
        return generateToken(user);
    }

    @Transactional
    public String verify(VerifyDto verifyDto) {
        Sms sms = smsCrud.findFirstByPhoneNumberAndCodeOrderByCreatedAtDesc(verifyDto.phoneNumber, verifyDto.code)
                .orElseThrow(InvalidVerify::new);
        if (!isValidSms(sms))
            throw new InvalidVerify();
        sms.getUser().setVerified(true);
        userCrud.save(sms.getUser());
        sms.setUsed(true);
        return generateToken(sms.getUser());
    }

    private boolean isValidSms(Sms sms) {
        return (sms.isSent() && !sms.isUsed());
    }


    public void forgotPassword(ForgotPasswordDto forgotPasswordDto) {
        User user = userCrud.findByPhoneNumber(forgotPasswordDto.phoneNumber)
                .orElseThrow(NotFoundException::new);
        smsCrud.save(generateSms(user, Sms.CreatedType.ForgetPassword));
    }

    public String resetPassword(ResetPasswordDto resetPasswordDto) {
        Sms sms = smsCrud.findFirstByPhoneNumberAndCodeAndCreatedAtGreaterThanEqualOrderByCreatedAtDesc(
                resetPasswordDto.phoneNumber,
                resetPasswordDto.code,
                LocalDateTime.now().minusDays(1)
        ).orElseThrow(InvalidVerify::new);
        if (isValidSms(sms))
            throw new InvalidVerify();
        User user = userCrud.findByPhoneNumber(resetPasswordDto.phoneNumber).orElseThrow(InvalidVerify::new);
        user.setVerified(true);
        user.setPassword(passwordEncoder.encode(resetPasswordDto.password));
        userCrud.save(user);
        return generateToken(user);
    }

    public void changePassword(User user, ChangePasswordDto changePasswordDto) {
        if (!passwordEncoder.matches(changePasswordDto.oldPassword, user.getPassword()))
            throw new InvalidVerify("old password incorrect");
        user.setPassword(passwordEncoder.encode(changePasswordDto.newPassword));
        userCrud.save(user);
    }

    private String generateToken(User user) {
        String[] roles = user.getRoles()
                .stream()
                .map(Role::getName)
                .toArray(String[]::new);
        return jwt.getBuilder()
                .withClaim("phoneNumber", user.getPhoneNumber())
                .withArrayClaim("roles", roles)
                .sign(algorithm);
    }

    @Transactional
    public Sms generateSms(User user, Sms.CreatedType createdType) {
        return new Sms().
                setCode(String.valueOf(RandomUtils.nextLong(1000_0, 1000_000 - 1)))
                .setPhoneNumber(user.getPhoneNumber())
                .setCreatedFor(createdType)
                .setUser(user);
    }

    public void authenticate(Scope scope, String token) {
        DecodedJWT decodeToken = jwt.verify(token);
        scope.setUser(userCrud
                .findByPhoneNumberAndVerified(decodeToken.getClaim("phoneNumber").asString(), true)
                .orElseThrow(UnAuthorized::new));
    }

    @Cached(timeUnit = TimeUnit.HOURS)
    public boolean hasAuthorized(Scope scope, Authorized authorized) {
        Iterable<Role> allRoles = roleRepo.findAll();
        List<Role> authorizedRoles = StreamSupport
                .stream(allRoles.spliterator(), false)
                .collect(Collectors.toList());
        return scope.getUser().getRoles()
                .stream()
                .anyMatch(r -> authorizedRoles.stream().anyMatch(x -> x.getPriority() >= r.getPriority()));
    }
}
