package journey.operation;

public class SolrType {
    public enum SolrTypeField {
        FILTER,
        NAVIGATIONAL,
        INCREMENT
    }
}
