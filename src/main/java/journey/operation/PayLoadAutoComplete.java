package journey.operation;

import java.util.List;

public interface PayLoadAutoComplete {
    List<PayLoadObjectSolr> payloadAutoComplete();
}
