package journey.operation;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import journey.persistence.crud.MediaCrud;
import journey.persistence.enitity.Media;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.lang.String.format;

@Service
public class MediaService {
    private final MediaCrud crud;
    private String storagePath;

    public MediaService(MediaCrud mediaRepo, @Value("${storage_path}") String storagePath) {
        this.crud = mediaRepo;
        this.storagePath = storagePath;
    }

    @Transactional
    public List<Media> storeImages(MultipartFile[] images, String modelName, Long modelId, String tableName) throws IOException {
        List<Media> medias = new ArrayList<>();
        for (MultipartFile image : images)
            medias.add(storeImage(image, modelName, modelId, tableName));
        return medias;
    }

    @Transactional
    public Media storeImage(MultipartFile image, String modelName, Long modelId, String tableName) throws IOException {
        Media media;
        String fileName = UUID.randomUUID().toString() + ".jpg";
        media = new Media()
                .setName(fileName)
                .setSize(image.getSize())
                .setTableName(tableName);
        crud.save(media);
        String path = format("%s/%s/%s/%s", storagePath, modelName, media.getId(), fileName);
        makeDir(path);
        media.setModelId(modelId).setModelName(modelName);
        media.setPath(path);
        media.setUrl(format("%s/%s", "media/images", media.getId()));
        crud.save(media);
        Path filepath = Paths.get(path, fileName);
        image.transferTo(filepath);
        return media;
    }

    @Transactional
    public List<Media> updateImages(MultipartFile[] image, String modelName, Long modelId, String tableName) throws IOException {
        List<Media> oldMedia = crud.findByModelIdAndModelNameAndTableName(modelId, modelName, tableName);
        oldMedia.forEach(f -> f.setDeleted(true));
        crud.saveAll(oldMedia);
        return storeImages(image, modelName, modelId, tableName);
    }

    private void makeDir(String dirName) {
        File file = new File(dirName);
        file.mkdirs();
    }

    public Media find(Long id) {
        return crud.findById(id).orElse(null);
    }
}
