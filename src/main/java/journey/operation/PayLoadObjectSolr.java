package journey.operation;

import lombok.Getter;

import java.util.HashMap;

public class PayLoadObjectSolr {
    @Getter
    private HashMap<String, Object> model = new HashMap<>();

    public PayLoadObjectSolr put(String name, Object value) {
        model.put(name, value);
        return this;
    }

}
