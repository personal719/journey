package journey.operation;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.stereotype.Service;
import journey.util.GlobalParams;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import static journey.operation.SolrType.SolrTypeField.INCREMENT;
import static journey.operation.SolrUtil.getAutocompleteCollection;

@Service
public class Document {
    private final SolrClient solrClient;
    private final int COMMIT_WITHIN;

    public Document(SolrClient solrClient, GlobalParams params) {
        this.solrClient = solrClient;
        COMMIT_WITHIN = params.getInt("commit_within");
    }

    public void indexing(List<PayLoadObjectSolr> payloads, String collectionName) {
        try {
            List<SolrInputDocument> documents;
            documents = payloads.stream()
                    .map(this::convertToSolrDocument)
                    .collect(Collectors.toList());
            solrClient.add(collectionName, documents, COMMIT_WITHIN);
        } catch (SolrServerException | IOException e) {
            e.printStackTrace();
        }
    }


    public void indexing(FullPayLoad fullPayLoad, String collectionName) {
        indexing(Collections.singletonList(fullPayLoad.payload()), collectionName);
        indexing(fullPayLoad.payloadAutoComplete(), getAutocompleteCollection(collectionName));
    }

    public void truncate(String collectionName) throws IOException, SolrServerException {
        solrClient.deleteByQuery(collectionName, "*:*", COMMIT_WITHIN);
    }

    public void delete(String collectionName, String id) {
        try {
            solrClient.deleteById(collectionName, id, COMMIT_WITHIN);
        } catch (SolrServerException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    private SolrInputDocument convertToSolrDocument(PayLoadObjectSolr payload) {
        SolrInputDocument document = new SolrInputDocument();

        payload.getModel().forEach((k, v) -> {
            if (INCREMENT.getClass().isInstance(v))
                document.addField(k, new HashMap<String, Object>() {{
                    put("inc", 1);
                }});
            else
                document.addField(k, v);
        });
        return document;
    }
}

