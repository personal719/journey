package journey.config;

import javafx.util.Pair;
import journey.persistence.enitity.Chalet;
import journey.persistence.enitity.Farm;
import journey.util.GlobalParams;
import journey.web.factory.OwnershipFactory;

import static java.lang.String.format;

public class OwnershipSetting {
    private static OwnershipSetting setting = null;
    private GlobalParams params;

    public static OwnershipSetting getInstance(GlobalParams params) {
        setting = new OwnershipSetting(params);
        return setting;
    }

    public static OwnershipSetting getInstance() {
        return setting;
    }

    public String translateAsArPlural(String type) {
        return params.get(format("%s_plural_ar", type));
    }

    private OwnershipSetting(GlobalParams params) {
        this.params = params;
        initOwnershipConverters();
    }

    private void initOwnershipConverters() {
        farmSetting();
        chaletSetting();
    }

    private void farmSetting() {
        Pair<String, String> createValidation = new Pair<>("create", "/validation/farm.json");
        Pair<String, String> update = new Pair<>("update", "/validation/update-farm.json");
        OwnershipFactory.addType(params.get("farm"), Farm.class, createValidation);
        OwnershipFactory.addType(params.get("farm_ar"), Farm.class, createValidation);
        OwnershipFactory.addType(params.get("farm"), Farm.class, update);
        OwnershipFactory.addType(params.get("farm_ar"), Farm.class, update);
    }

    private void chaletSetting() {
        Pair<String, String> createValidation = new Pair<>("create", "/validation/chalet.json");
        Pair<String, String> update = new Pair<>("update", "/validation/update_chalet.json");
        OwnershipFactory.addType(params.get("chalet"), Chalet.class, createValidation);
        OwnershipFactory.addType(params.get("chalet_ar"), Chalet.class, createValidation);
        OwnershipFactory.addType(params.get("chalet"), Chalet.class, update);
        OwnershipFactory.addType(params.get("chalet_ar"), Chalet.class, update);
    }

}
