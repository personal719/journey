CREATE TABLE users(
  id int primary key auto_increment,
  username varchar (50) not null,
  password varchar (300) not null,
  phone_number varchar(50) not null,
  verified boolean default 0,
  CREATEd_at timestamp  default current_timestamp,
  updated_at timestamp  default current_timestamp
);

CREATE TABLE media(
  id int primary key auto_increment,
  name varchar (50) not null,
  path varchar (100),
  size double ,
  model_name varchar (100),
  model_id varchar (100),
  table_name varchar (100),
  url varchar (300),
  is_deleted boolean default false,
  CREATEd_at timestamp  default current_timestamp,
  updated_at timestamp  default current_timestamp
);
CREATE TABLE ownership(
    id int primary key auto_increment,
    name varchar (50),
    media_id int,
    description varchar (255),
    type varchar (50),
    active boolean,
    phone_number varchar (50),
    governate varchar (50),
    region varchar (50),
    user_id int,
    is_deleted boolean default false,
    is_activate boolean default true,
    created_at timestamp  default current_timestamp,
    updated_at timestamp  default current_timestamp
);
CREATE TABLE chalet(
    id int primary key auto_increment,
    chalet_state varchar (50),
    ownership_id int not null
);

CREATE TABLE farm(
    id int primary key auto_increment,
    ownership_id int not null,
    has_pool boolean,
    has_court boolean,
    depth_pool boolean
);
CREATE TABLE journey(
    id int primary key auto_increment,
    ownership_id int not null,
    started_at date not null,
    end_at date not null,
    from_region varchar (50),
    to_region varchar (50),
    created_at timestamp  default current_timestamp,
    updated_at timestamp  default current_timestamp
);
create TABLE sms(
    id int primary key auto_increment,
    visitor_session varchar (300),
    code varchar (50),
    is_used boolean,
    is_sent boolean,
    created_for varchar (50),
    phone_number varchar (100),
    create_for varchar (100),
    user_id int,
    created_at timestamp  default current_timestamp,
    updated_at timestamp  default current_timestamp
);
create table log_req
(
    id int primary key auto_increment,
    token varchar (300),
    visitor_session varchar (300),
    country varchar (100),
    city varchar (100),
    ip_address varchar (100),
    device_type varchar (100),
    processing_period double,
    status long  not null,
    uri varchar (300),
    host varchar (100),
    exception varchar (100),
    params varchar (300),
    method varchar (100),
    created_at timestamp  default current_timestamp,
    updated_at timestamp  default current_timestamp
);
create table log_search
(
    id int primary key auto_increment,
    query varchar (100),
    processing_period double,
    num_result int,
    is_statisfied boolean,
    created_at timestamp  default current_timestamp,
    updated_at timestamp  default current_timestamp
);
create TABLE role
(
    id int primary key auto_increment,
    name varchar (100) not null,
    priority int not null,
    created_at timestamp  default current_timestamp,
    updated_at timestamp  default current_timestamp
);
CREATE table user_role
(
    id int primary key auto_increment,
    user_id int not null,
    role_id int not null
);
ALTER TABLE chalet ADD foreign key (ownership_id) references ownership(id);
ALTER TABLE journey ADD foreign key (ownership_id) references ownership(id);
ALTER TABLE farm ADD foreign key (ownership_id) references ownership(id);
ALTER TABLE user_role ADD foreign key (role_id) references role(id);
ALTER TABLE user_role ADD foreign key (user_id) references users(id);
ALTER TABLE sms ADD foreign key (user_id) references users(id);
ALTER TABLE ownership ADD foreign key (user_id) references users(id);


insert into role(name,priority) value ('ADMIN',1);
insert into role(name,priority) value ('USER',2);
insert into users(username,phone_number,password,verified) value ('hazem','09371249459','$2a$10$OcAvyaoWAQFJfqgMngtkjOt0Zqk71MqMWo/ixOVQX1ybmif1r4LrO',true);
insert into user_role(user_id,role_id) value (1,1);
insert into user_role(user_id,role_id) value (1,2);